@extends('layouts.app')

@section('content')
    <style>
        #chart_dashboard {
            width: 100%;
            height: 400px;
            margin: 0 auto
        }
    </style>
    <h1>Dashboard</h1>
    <div id="chart_dashboard"></div>
    <script>

        $('.module-menu > a').each(function(){
                $(this).children().removeClass('color--active');
            });
        $("#lnk_dashboard").children().addClass('color--active');
        
        Highcharts.chart('chart_dashboard', {

            title: {
                text: 'Truck Employment Growth by Sector, 2010-2017'
            },

            subtitle: {
                text: 'Source: thetruckfoundation.com'
            },

            yAxis: {
                title: {
                    text: 'Number of Employees'
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },

            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                    pointStart: 2010
                }
            },

            series: [{
                name: 'Installation',
                data: [33934, 32503, 37177, 39658, 47031, 49931, 57133, 47175]
            }, {
                name: 'Manufacturing',
                data: [24916, 24064, 29742, 29851, 32490, 30282, 38121, 40434]
            }, {
                name: 'Sales & Distribution',
                data: [11744, 17722, 16005, 19771, 20185, 24377, 32147, 39387]
            }, {
                name: 'Project Development',
                data: [null, null, 7988, 12169, 15112, 22452, 34400, 34227]
            }, {
                name: 'Other',
                data: [12908, 5948, 8105, 11248, 8989, 11816, 18274, 18111]
            }],

            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }

        });
    </script>
@endsection