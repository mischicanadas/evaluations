@extends('layouts.app')

@section('content')
    <h1>Pending</h1>
    <table class="table table-condensed table-hover">
        <thead>
            <tr>
                <th>#</th>
                <th>Evaluation</th>
                <th>Start</th>
                <th>End</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>1</td>
                <td>Know your workplace</td>
                <td>12/10/2017</td>
                <td>12/25/2017</td>
                <td>
                    <div class="progress">
                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 85%">
                            <span>85%</span>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>2</td>
                <td>Equipment: Basics</td>
                <td>12/10/2017</td>
                <td>12/25/2017</td>
                <td>
                    <div class="progress">
                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 30%">
                            <span>30%</span>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>3</td>
                <td>Equipment: Advanced</td>
                <td>12/10/2017</td>
                <td>12/25/2017</td>
                <td>
                    <div class="progress">
                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                            <span>0%</span>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>4</td>
                <td>HR and you: Benefits</td>
                <td>12/10/2017</td>
                <td>12/25/2017</td>
                <td>
                    <div class="progress">
                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100" style="width: 15%">
                            <span>15%</span>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>5</td>
                <td>Safe driver</td>
                <td>12/10/2017</td>
                <td>12/25/2017</td>
                <td>
                    <div class="progress">
                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                            <span>70%</span>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <script>

        $('.module-menu > a').each(function(){
                $(this).children().removeClass('color--active');
            });
        $("#lnk_pending").children().addClass('color--active');
        
    </script>
@endsection