<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <script src="{{ asset('js/app.js') }}"></script>

        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/series-label.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>

        <title>{{ config('app.name','Evaluations') }}</title>
    </head>
    <body>
        <header class="site-header d-block text-center w-100">
            <div class="logo pull-left d-inline-block mx-auto">
                <img class="logo-img pl-3" src="img/ContactCenterSolutionsLogo.png" />
            </div>
            <div class="pull-right">
                <div class="company-menu">
                <i class="ion-ios-person-outline"></i><span class="company__name">My Trucking Company, Inc<i class="ion-chevron-right"></i> Luis Gomez</span>
                </div>
                <div class="main-menu-toggle pull-right module-menu-close color--rojo">
                <i class="ion-ios-close-outline"></i>
                </div>
            </div>
            <div class="module-menu col-md-12 no-pad clearfix px-0">
                <a id="lnk_dashboard" href="dashboard">
                <div class="module-item color--azul col-xs-2 col-sm-2 col-md-2 no-pad pull-left text-center">
                    <i class="ion-ios-pulse module-item__icon d-block"></i><span class="module-item__text d-block">Dashboard</span>
                </div>
                </a>
                <a id="lnk_evaluations" href="evaluations">
                <div class="module-item color--azul col-xs-2 col-sm-2 col-md-2 no-pad pull-left text-center">
                    <i class="ion-ios-settings module-item__icon d-block"></i><span class="module-item__text d-block">Evaluations</span>
                </div>
                </a>
                <a id="lnk_pending" href="pending">
                <div class="module-item color--azul col-xs-2 col-sm-2 col-md-2 no-pad pull-left text-center">
                    <i class="ion-ios-loop module-item__icon d-block"></i><span class="module-item__text d-block">Pending</span>
                </div>
                </a>
                <a id="lnk_archive" href="archive">
                <div class="module-item color--azul col-xs-2 col-sm-2 col-md-2 no-pad pull-left text-center">
                    <i class="ion-ios-box-outline module-item__icon d-block"></i><span class="module-item__text d-block">Archive</span>
                </div>
                </a>
                <a id="lnk_help" href="help">
                <div class="module-item color--azul col-xs-2 col-sm-2 col-md-2 no-pad pull-left text-center">
                    <i class="ion-ios-help-outline module-item__icon d-block"></i><span class="module-item__text d-block">Help</span>
                </div>
                </a>
                <a id="lnk_configuration" href="configuration">
                <div class="module-item color--azul col-xs-2 col-sm-2 col-md-2 no-pad pull-left text-center">
                    <i class="ion-ios-gear-outline module-item__icon d-block"></i><span class="module-item__text d-block">Configuration</span>
                </div>
                </a>
            </div>
        </header>
        <main class="container-fluid wrap">
            <div id="div_contenido" class="inner-wrap">
                @yield('content')
            </div>
        </main>
        <script src="{{asset('js/custom.js')}}"></script>
        <script src="{{asset('js/jquery.validate.min.js')}}"></script>
        <script src="{{asset('js/jquery.bootstrap.js')}}"></script>
        <script src="{{asset('js/material-bootstrap-wizard.js')}}"></script>
    </body>
</html>