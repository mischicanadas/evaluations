$(document).ready(function(){
    
    console.log('Custom js file');

    $("#lnk_dashboard").on("click",function(e){
        //e.preventDefault();
        menu_click($(this));
    });

    $("#lnk_evaluations").on("click",function(e){
        //e.preventDefault();
        menu_click($(this));
    });

    $("#lnk_pending").on("click",function(e){
        //e.preventDefault();
        menu_click($(this));
    });

    function menu_click(lnk)
    {
        $('.module-menu > a').each(function(){
            $(this).children().removeClass('color--active');
        });
        $(lnk).children().addClass('color--active');
    }
    
});