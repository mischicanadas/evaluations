<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    //Index
    public function index()
    {
        return view('index');
    }

    //Dashboard
    public function dashboard()
    {
        return view('dashboard');
    }

    //Evaluations
    public function evaluations()
    {
        return view('evaluations');
    }

    //Pending
    public function pending()
    {
        return view('pending');
    }

    //Archive
    public function archive()
    {
        return view('archive');
    }

    //Help
    public function help()
    {
        return view('help');
    }

    //Configuration
    public function configuration()
    {
        return view('configuration');
    }
}
